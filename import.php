<?php
$file = fopen('var/import/product.csv', 'r', '"'); // set path to the CSV file
if ($file !== false) {
    require __DIR__ . '/app/bootstrap.php';
    $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('adminhtml');
    /*$storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
    $storeManager->SetStoreId(13);*/
    //exit;
    $galleryReadHandler = $objectManager->create('Magento\Catalog\Model\Product\Gallery\ReadHandler');
    $imageProcessor = $objectManager->create('Magento\Catalog\Model\Product\Gallery\Processor');
    $productGallery = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Gallery');
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/import-product.log');
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);

    $header = fgetcsv($file); // get data headers and skip 1st row
    $required_data_fields = 3;

    while ($row = fgetcsv($file, 3000, ",")) {
        $data_count = count($row);
        if ($data_count < 1) {
            continue;
        }
        $data = array();
        $data = array_combine($header, $row);
        $sku = $data['sku'];
        if ($data_count < $required_data_fields) {
            $logger->info("Skipping product sku " . $sku . ", not all required fields are present to create the product.");
            continue;
        }
        $product = $objectManager->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $sku);
        if ($product) {
            $galleryReadHandler->execute($product);
            //echo '<pre>'; print_r($product->getData()); exit;
            //$product = $objectManager->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $sku);
            $imagelabel = trim($data['additional_image_label']);

            //$attributes = $product->getTypeInstance(true)->getSetAttributes($product);
            $images = $product->getMediaGallery('images');
            //echo '<pre>'; print_r($images); exit;
            //$mediaGalleryBackendModel = $attributes['media_gallery']->getBackend();
            foreach ($images as $image) {
                echo '<pre>';
                print_r($images);
                exit;
                $mediaGalleryBackendModel->updateImage($product, $image->getFile(), array('label' => $imagelabel));
            }
            //exit;
            $product->save();
        }
    }
    //echo $sku;
    //echo $imagelabel;
    //exit;
    fclose($file);
}
