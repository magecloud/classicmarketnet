<?php

function p($obj)
{
    echo "<pre>";
    print_r($obj);
    echo "</pre>";
    exit;
}

function pp($obj)
{
    echo "<pre>";
    print_r($obj);
    echo "</pre>";
}

if ( isset($_POST["submit"]) ) {
    if ( isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } elseif($_FILES["file"]["type"] == "text/csv" || $_FILES["file"]["type"] == "application/vnd.ms-excel") {
        	$csv_file = $_FILES["file"]["tmp_name"];
            $handle      = fopen($csv_file, "r");
            $destination = $_POST['download_path'];
            $row = 0;
            $images = 0;
            $fileNameIndex = 0;
            $productTypeColumnIndex;
            $additionalImagesColumnIndex;
            $foundInCsvColumns = array();
            $writeArray = array();
            $imageHeader = array();
            if ($handle) {
                while ($columns = fgetcsv($handle, 0, ',')) {
                    if($row == 0) {
                        if(empty(trim($_POST['image_column']))) {
                            echo "<span style='color:red;'>Please enter column name(s) to download images from</span><br/>";
                            echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
                            exit();
                        } else {
                            $productTypeColumnIndex = array_search('product_type', $columns);
                            $additionalImagesColumnIndex = array_search('additional_images', $columns);
                            $targetedColumns = explode(",", $_POST['image_column']);
                            $missingColumns = array_diff($targetedColumns, $columns);
                            if(empty($missingColumns)) {
                                $foundInCsvColumns = array_keys(array_intersect($columns, $targetedColumns));
                                $imageHeader[] = $_POST['image_name'];

                                $imageHeader = array_merge($imageHeader,array_intersect($columns, $targetedColumns));
                                $writeArray[] = $imageHeader;
                                ob_start();
                                echo "<span style='color:green;'>You choose to download images from column <b>|".implode(",", $targetedColumns)."|</b><br/>";
                                ob_end_clean();
                            } else {
                                echo "<span style='color:red;'>Please check following column is either missing in CSV file or Spelling is correct :- <br/><br/><b>".implode(",", $missingColumns)."</b></span><br/>";
                                echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
                                exit();
                            }

                            if(empty(trim($_POST['download_path']))) {
                                echo "<span style='color:red;'>Please enter <b>Image Download Directory</b></span><br/>";
                                echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
                                exit();   
                            }

                            if(empty(trim($_POST['image_name']))) {
                                echo "<span style='color:red;'>Please enter <b>Image name based on column</b></span><br/>";
                                echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
                                exit();   
                            } else {
                                $fileNameIndex = array_search(trim($_POST['image_name']), $columns);
                                if($fileNameIndex === false) {
                                    echo "<span style='color:red;'><b>Image name based on column</b> not found in CSV file</span><br/>";
                                    echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
                                    exit();
                                }
                            }
                        }
                    } else {
                        //if($columns[$productTypeColumnIndex] == "configurable") {
                            $imagename = str_replace("/", "_", strtolower($columns[$fileNameIndex]));
                            $imagename = preg_replace('/[^a-zA-Z0-9\-_]/', '', $imagename);

                            $newArray = array();
                            $newArray[] = $columns[$fileNameIndex];
                            $imageCounter = 0;
                            $url = "";
                            foreach ($foundInCsvColumns as $columnIndex) {
                                if((int)trim($columnIndex) == (int)trim($additionalImagesColumnIndex)) {
                                    $additionalImageArray = explode(",", $columns[$columnIndex]);
                                    $newAdditionalImageArray = array();
                                    $additionalImageCounter = 0; // change it to 1

                                    foreach ($additionalImageArray as $additionalImageUrl) {
                                    	if($additionalImageCounter == 0) {
	                                        $additionalImageCounter++;
	                                        continue;
	                                    }
                                        $urlArray = parse_url($additionalImageUrl);
                                        if(isset($urlArray["query"])) {
                                            $url = str_replace($urlArray["query"], "", $additionalImageUrl);
                                            $url = str_replace("?", "", $url);
                                        } else {
                                            $url = explode("?", $additionalImageUrl)[0];
                                        } 
                                        $fileInfo = pathinfo($url);

                                        if(!empty($url)) {
                                            if(isset($fileInfo['extension']) && !empty($fileInfo['extension'])) {
                                                $ext = $fileInfo['extension'];
                                            } elseif(!empty(pathinfo($url, PATHINFO_EXTENSION))) {
                                                $ext = pathinfo($url, PATHINFO_EXTENSION);
                                            }
                                            $ext = trim($ext);
                                            if(!empty($ext)) {
                                                $newAdditionalImageArray[] = $imagename.$additionalImageCounter.".".$ext;
                                                if(!file_exists($destination."/".$imagename.$additionalImageCounter.".".$ext)) {
                                                    ob_start();
                                                    $header = array("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8");
                                                    $options = array(
                                                        CURLOPT_HTTPHEADER => $header,
                                                        CURLOPT_ENCODING => "gzip",
                                                        CURLOPT_CUSTOMREQUEST => 'GET',
                                                        CURLOPT_RETURNTRANSFER => true,
                                                        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
                                                        CURLOPT_FRESH_CONNECT => true,
                                                    );
                                                    $ch = curl_init($url);
                                                    curl_setopt_array($ch, $options);
                                                    curl_setopt($ch, CURLOPT_HEADER, 0);
                                                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
                                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                                    $raw=curl_exec($ch);
                                                    curl_close ($ch);
                                                    $fp = fopen($destination."/".$imagename.$additionalImageCounter.".".$ext,'x');
                                                    fwrite($fp, $raw);
                                                    fclose($fp);
                                                    ob_end_clean();
                                                }
                                                $additionalImageCounter++;
                                            }
                                        }
                                        unset($additionalImageUrl);   
                                    }
                                    if(!empty($newAdditionalImageArray)){
                                        $allAdditionalImages = implode(",", $newAdditionalImageArray);
                                        $newArray[] = $allAdditionalImages;
                                    }
                                } else {
                                    $images++;
                                    $urlArray = parse_url($columns[$columnIndex]);
                                    
                                    if(isset($urlArray["query"])) {
                                    	$url = str_replace($urlArray["query"], "", $columns[$columnIndex]);
                                    	$url = str_replace("?", "", $url);
                                    } else {
                                    	$url = explode("?", $columns[$columnIndex]);
                                        if(!empty($url)) {
                                            $url= $url[0];
                                        }
                                    } 
                                    $fileInfo = pathinfo($url);

                                    if(!empty($url)) {
        	                            if(isset($fileInfo['extension']) && !empty($fileInfo['extension'])) {
        	                                $ext = $fileInfo['extension'];
        	                            } elseif(!empty(pathinfo($url, PATHINFO_EXTENSION))) {
        									$ext = pathinfo($url, PATHINFO_EXTENSION);
        	                            }
        	                            $ext = trim($ext);
        	                            if(!empty($ext)) {
                                    		$newArray[] = $imagename.$imageCounter.".".$ext;
                                            $saveTo = $destination."/".$imagename.$imageCounter.".".$ext;
                                            if(!file_exists($saveTo)) {
                                                ob_start();
                                                $header = array("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8");
                                                $options = array(
                                                    CURLOPT_HTTPHEADER => $header,
                                                    CURLOPT_ENCODING => "gzip",
                                                    CURLOPT_CUSTOMREQUEST => 'GET',
                                                    CURLOPT_RETURNTRANSFER => true,
                                                    CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
                                                    CURLOPT_FRESH_CONNECT => true,
                                                );
                                                $ch = curl_init($url);
                                                curl_setopt_array($ch, $options);
                                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
                                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                                $raw=curl_exec($ch);
                                                curl_close ($ch);
                                                $fp = fopen($saveTo,'x');
                                                fwrite($fp, $raw);
                                                fclose($fp);
                                                ob_end_clean();
        		                            }
        	                            } else {
                                    		$newArray[] = "There is an issue loading image";
        	                            }
                                    } else {
                                        if(file_exists($destination."/".$imagename."0.".$ext)) {
                                            $newArray[] = $imagename."0.".$ext;
                                        } else {
                                    	   $newArray[] = "URL Path is missing";
                                        }
                                    }
                                }
                            }
                            $writeArray[] = $newArray;
                            unset($newArray);
                        //} else {
                            //$writeArray[] = array("","","","","");
                        //}
                    }
                    $row++;
                }
                
                $writeHandle      = fopen('php://memory', "w");
                foreach ($writeArray as $data) {
                    fputcsv($writeHandle, $data);
                }
                fseek($writeHandle, 0);
                header('Content-Type: application/csv');
                header('Content-Disposition: attachment; filename="'.$_POST['export_file_name'].'";');
                fpassthru($writeHandle);
                fclose($writeHandle);
            }
        } else {
            echo "Please upload CSV File<br />";   
        }
    }
    ob_start();
    echo "<br/><span style='color:green'>Total <b>".($row - 1)." row(s)</b> processed and <b>".$images." Image(s)</b> downloaded successfully</span><br>";
    echo "<input type='button' onclick='location.href=window.location.href' value='Go Back'>";
    ob_end_clean();
} else {
?>
    <table width="680" border="1px">
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
            <tr>
                <td width="58%">Select file</td>
                <td width="2%">:-</td>
                <td width="40%"><input type="file" name="file" id="file" /></td>
            </tr>
            <tr>
                <td width="58%">Enter image field name by comma seperated<br/><b>Example :- base_image,small_image,thumbnail_image</b></td>
                <td width="2%">:-</td>
                <td width="40%"><input type="text" name="image_column" id="image_column" /></td>
            </tr>
            <tr>
                <td width="58%">Image Download Directory<br/><b>Suggested :- pub/media/import</b></td>
                <td width="2%">:-</td>
                <td width="40%"><input type="text" name="download_path" id="download_path" /></td>
            </tr>
            <tr>
                <td width="58%">Image name based on column<br/><b>Suggested :- sku</b></td>
                <td width="2%">:-</td>
                <td width="40%"><input type="text" name="image_name" id="image_name" /></td>
            </tr>
            <tr>
                <td width="58%">Enter Image File name to Export<br/><b>Example :- xxxxxx.csv</b></td>
                <td width="2%">:-</td>
                <td width="40%"><input type="text" name="export_file_name" id="export_file_name" /></td>
            </tr>
            <tr>
                <td colspan="3"><input type="submit" name="submit" value="Download Image(s)" /></td>
            </tr>
        </form>
    </table>

<?php
}

