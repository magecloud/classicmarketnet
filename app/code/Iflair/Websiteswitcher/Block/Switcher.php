<?php
namespace Iflair\Websiteswitcher\Block;

class Switcher extends \Magento\Framework\View\Element\Template
{
    
    public function getWebsites() {
        if (!$this->getData('websites')) {
            $websites = $this->_storeManager->getWebsites();
            return $websites;
        }
    }

    public function getCurrentWebsiteId()
    {
        return $this->_storeManager->getWebsite()->getId();
    }

}
