<?php

declare(strict_types=1);
namespace Iflair\Catalog\Model\ResourceModel;

class Category extends \Magento\Catalog\Model\ResourceModel\Category
{
   public function getChildrenCategories($category)
   {
       $collection = $category->getCollection();
       /* @var $collection \Magento\Catalog\Model\ResourceModel\Category\Collection */
       $collection->addAttributeToSelect('url_key')
                  ->addAttributeToSelect('name')
                  ->addAttributeToSelect('all_children')
                  ->addAttributeToSelect('is_anchor')
                  ->addAttributeToFilter('is_active',1)
                  ->addIdFilter($category->getChildren())
                  ->setOrder('name',\Magento\Framework\DB\Select::SQL_ASC)
                  ->joinUrlRewrite();
       return $collection;
   }
}
?>