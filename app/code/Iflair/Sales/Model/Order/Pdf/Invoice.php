<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Iflair\Sales\Model\Order\Pdf;

    use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection;
    use Magento\Sales\Model\Order\Pdf\AbstractPdf;
    use Magento\Sales\Model\Order\Pdf\Config;
    use Magento\Sales\Model\Order\Pdf\Invoice as MagentoInvoice;
    //use Magento\Directory\Api\CountryInformationAcquirerInterface;


/**
 * Sales Order Invoice PDF model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Invoice extends MagentoInvoice
    {

        /**
         * Draw header for item table
         *
         * @param \Zend_Pdf_Page $page
         * @return void
         */
      
        protected function _drawHeader(\Zend_Pdf_Page $page)
        {
            /* Add table head */
            //$this->_setFontArial($page, 10);
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
            $page->drawRectangle(25, $this->y, 570, $this->y - 15);
            $this->y -= 10;
            $page->setFillColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));

            //columns headers
            $lines[0][] = ['text' => __('Código'), 'feed' => 35,'font' => 'bold'];

            $lines[0][] = ['text' => __('Descripción'), 'feed' => 120,'font' => 'bold'];

            
            
            $lines[0][] = ['text' => __('Cant'), 'feed' => 475, 'align' => 'right','font' => 'bold'];

            $lines[0][] = ['text' => __('Precio'), 'feed' => 380, 'align' => 'right','font' => 'bold'];

            //$lines[0][] = ['text' => __('Tax'), 'feed' => 495, 'align' => 'right'];

            $lines[0][] = ['text' => __('Subtotal'), 'feed' => 565, 'align' => 'right','font' => 'bold'];

            $lineBlock = ['lines' => $lines, 'height' => 5];

            $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
            $this->y -= 20;
        }

        /**
         * Return PDF document
         *
         * @param array|Collection $invoices
         * @return \Zend_Pdf
         */
        public function getPdf($invoices = [])
        {
            $this->_beforeGetPdf();
            $this->_initRenderer('invoice');

            $pdf = new \Zend_Pdf();
            $this->_setPdf($pdf);
            $style = new \Zend_Pdf_Style();
            $this->_setFontBold($style, 10);

            foreach ($invoices as $invoice) {
                if ($invoice->getStoreId()) {
                    $this->_localeResolver->emulate($invoice->getStoreId());
                    $this->_storeManager->setCurrentStore($invoice->getStoreId());
                }

                $page = $this->newPage();
                $order = $invoice->getOrder();
                /* Add image */
                $this->insertLogo($page, $invoice->getStore());
                /* Add address */
                $this->insertAddress($page, $invoice->getStore());
                /* Add head */
                $this->insertOrder(
                    $page,
                    $order,
                    $this->_scopeConfig->isSetFlag(
                        self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $order->getStoreId()
                    )
                );
                /* Add document text and number */
                $this->insertDocumentNumber($page, __('Factura No # ') . $invoice->getIncrementId());
                /* Add table */
                $this->_drawHeader($page);
                /* Add body */
                foreach ($invoice->getAllItems() as $item) {
                    if ($item->getOrderItem()->getParentItem()) {
                        continue;
                    }
                    /* Draw item */
                    $this->_drawItem($item, $page, $order);
                    $page = end($pdf->pages);
                }
                /* Add totals */
                $this->insertTotals($page, $invoice);
                if ($invoice->getStoreId()) {
                    $this->_localeResolver->revert();
                }

              /*  foreach ($invoice->getOrder()->getStatusHistoryCollection() as $status){
                    $textChunk = wordwrap(strip_tags($status->getComment()), 120, "\n");
                    foreach(explode("\n", $textChunk) as $textLine){
                        if ($textLine!=='') {
                            $page->drawText(strip_tags(ltrim($textLine)), 35, $this->y, 'UTF-8');
                            $this->y -= 15;
                        }
                    }
                }*/
            }
            $this->_drawFooter($page);
            $this->_afterGetPdf();
            return $pdf;
        }


        /**
         * Create new page and assign to PDF object
         *
         * @param  array $settings
         * @return \Zend_Pdf_Page
         */
        public function newPage(array $settings = [])
        {
            /* Add new table head */
            $page = $this->_getPdf()->newPage(\Zend_Pdf_Page::SIZE_A4);
            $this->_getPdf()->pages[] = $page;
            $this->y = 800;
            if (!empty($settings['table_header'])) {
                $this->_drawHeader($page);
            }
            return $page;
        }
        protected function _drawFooter(\Zend_Pdf_Page $page)
        {   
            $this->y =30;    
            $page->setFillColor(new \Zend_Pdf_Color_RGB(1, 1, 1));
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
            //$page->drawRectangle(60, $this->y, 510, $this->y -30);

            $page->setFillColor(new \Zend_Pdf_Color_RGB(0.1, 0.1, 0.1));
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 7);
            $this->y +=80;
            $line = 150;
            $value = 'Cover Company - Julián Gustavo Fontana NIF: X1553802Z - IVA Intracomunitario / EORI: ESX1553802Z / GB EORI: GB075442704000 Adriá Gual 5 1ºB 08173 - Sant Cugat del Vallés - Barcelona - España';
            $textChunk = wordwrap($value, 125, "\n");
            foreach(explode("\n", $textChunk) as $textLine){
              if ($textLine!=='') {
                $page->drawText(strip_tags(ltrim($textLine)), 30, $line, 'UTF-8');
                $line -=14;
              }
            }
            //$page->drawText("Cover Company - Julián Gustavo Fontana Adriá Gual 5 1ºB 08173 - Sant Cugat del Vallés Datos Bancarios: Banco Sabadell - IBAN: ES1600810432060001120618", 70, $this->y, 'UTF-8');
            //$page->drawText("Adriá Gual 5 1ºB 08173 - Sant Cugat del Vallés", 230, $this->y, 'UTF-8','<br/>');
            $page->drawText("Datos Bancarios: Banco Sabadell - IBAN: ES1600810432060001120618 - BIC: BSAB ESBB Paypal: https://paypal.me/covercompany", 30, 120, 'UTF-8');
            /*$companyName = '<a href="https://paypal.me/covercompany">Paypal</a>';
            $page->drawText($companyName, 120, 100, 'UTF-8');*/
            
            
        }
        public function insertDocumentNumber(\Zend_Pdf_Page $page, $text)
        {
            //$page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
            //$this->_setFontRegular($page, 10);
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);
            $docHeader = $this->getDocHeaderCoordinates();
            $page->drawText($text, 350, $docHeader[1] + 60, 'UTF-8');
        }
        protected function insertOrder(&$page, $obj, $putOrderId = true)
        {
                if ($obj instanceof \Magento\Sales\Model\Order) {
                    $shipment = null;
                    $order = $obj;
                } elseif ($obj instanceof \Magento\Sales\Model\Order\Shipment) {
                    $shipment = $obj;
                    $order = $shipment->getOrder();
                }

                $this->y = $this->y ? $this->y : 815;
                $top = $this->y;

                //$page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.45));
                //$page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.45));
                //$page->drawRectangle(25, $top, 570, $top - 55);
                //$page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
                $this->setDocHeaderCoordinates([25, $top, 570, $top - 55]);
                //$this->_setFontRegular($page, 10);
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);

                $top +=45;
                $page->drawText(
                    __('Fecha: ') .
                    $this->_localeDate->formatDate(
                        $this->_localeDate->scopeDate(
                            $order->getStore(),
                            $order->getCreatedAt(),
                            true
                        ),
                        \IntlDateFormatter::SHORT,
                        false
                    ),
                    350,
                    $top,
                    'UTF-8'
                );

                if ($putOrderId) {
                    $page->drawText(__('Pedido No # ') . $order->getRealOrderId(), 350, $top -= 15, 'UTF-8');
                    $top +=0;
                }


                $top -= 10;
                //$page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                //$page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
                //$page->setLineWidth(0.5);
                //$page->drawRectangle(25, $top, 275, $top - 25);
                //$page->drawRectangle(275, $top, 570, $top - 25);

                /* Calculate blocks info */

                /* Billing Address */
                $billingAddress = $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));
                /*$string = $billingAddress[4];
                $array = explode(', ', $string);
                $giberish = $array[2];
                echo $giberish; exit;*/
                //echo '<pre>'; print_r(count($billingAddress)); exit;
                

                /* Payment */
                $paymentInfo = $this->_paymentData->getInfoBlock($order->getPayment())->setIsSecureMode(true)->toPdf();
                $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
                $payment = explode('{{pdf_row_separator}}', "Forma de Pago: " . $paymentInfo);
                foreach ($payment as $key => $value) {
                    if (strip_tags(trim($value)) == '') {
                        unset($payment[$key]);
                    }
                }
                reset($payment);

                /* Shipping Address and Method */
                if (!$order->getIsVirtual()) {
                    /* Shipping Address */
                   /* $shippingAddress = $this->_formatAddress(
                        $this->addressRenderer->format($order->getShippingAddress(), 'pdf')
                    );*/
                    //$shippingMethod = $order->getShippingDescription();
                }

                $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
                
                //$page->drawText(__('Sold to:'), 35, $top - 15, 'UTF-8');

                if (!$order->getIsVirtual()) {
                    //$page->drawText(__('Ship to:'), 285, $top - 15, 'UTF-8');
                } else {
                    //$page->drawText(__('Payment Method:'), 285, $top - 15, 'UTF-8');
                }

                $addressesHeight = $this->_calcAddressHeight($billingAddress);
                if (isset($shippingAddress)) {
                    $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
                }

                $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
                //$page->drawRectangle(25, $top - 25, 570, $top - 33 - $addressesHeight);
                $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);
                //$this->_setFontRegular($page, 10);
                $this->y = $top - 40;
                $addressesStartY = $this->y;

                /*foreach ($billingAddress as $value) {
                    if ($value !== '') {
                        $text = [];
                        foreach ($this->string->split($value, 45, true, true) as $_value) {
                            $text[] = $_value;
                        }
                        foreach ($text as $part) {
                            $page->drawText(strip_tags(ltrim($part)), 35, $this->y, 'UTF-8');
                            $this->y -= 15;
                        }
                    }
                }*/
                /*[0] => julian fonta
                [1] => Company Name
                [2] => address line 1
                [3] => address line 2
                [4] => London, Province 2, 5DX 4FRR
                [5] => United Kingdom
                [6] => T: 983324323*/
              
               
                //echo $bill; exit;
                $billingAddress = $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));
                 //echo '<pre>'; print_r($order->getBillingAddress()->getData()); exit;
                 /*$postcode = 'PostCode: '.$order->getBillingAddress()->getData('postcode');
                    if($postcode != null)
                    {
                        array_push($billingAddress,$postcode);
                    }*/
                
                $firstname = $order->getBillingAddress()->getData("firstname");
                $lastname = $order->getBillingAddress()->getData("lastname");
                $page->drawText("Cliente: ".$firstname." ".$lastname, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                $company = $order->getBillingAddress()->getData("company");
                 if (!empty($company)) {
                $page->drawText("Empresa: ".$company, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                }
                $street = $order->getBillingAddress()->getData("street");
                $street1 = $street[0] ."". $street[1];
                $page->drawText("Dirección: ". $street, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                //echo '<pre>'; print_r($street); exit;
                $city = $order->getBillingAddress()->getData("city");
                $state = $order->getBillingAddress()->getData("region");
                $page->drawText("Ciudad: ".$city.",".$state, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $countryFactory = $objectManager->get('Magento\Directory\Model\CountryFactory')->create();
                $countryIdBilling = $order->getBillingAddress()->getCountryId();
                $country = $countryFactory->loadByCode($countryIdBilling);
                $countryName = $country->getName();
                $page->drawText("Pais: ".$countryName, 35, $this->y, 'UTF-8');
                $this->y -= 15; 
                $postcode = $order->getBillingAddress()->getData("postcode");
                $page->drawText("Código Postal: ".$postcode, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                $tax = $order->getBillingAddress()->getData("mposc_field_1");
                $page->drawText("NIF / CIF: ". $tax, 35, $this->y, 'UTF-8');
                $this->y -= 15;
                
                $addressesEndY = $this->y;

                if (!$order->getIsVirtual()) {
                    $this->y = $addressesStartY;
                    /*foreach ($shippingAddress as $value) {
                        if ($value !== '') {
                            $text = [];
                            foreach ($this->string->split($value, 45, true, true) as $_value) {
                                $text[] = $_value;
                            }
                            foreach ($text as $part) {
                                $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
                                $this->y -= 15;
                            }
                        }
                    }*/

                    $addressesEndY = min($addressesEndY, $this->y);
                    $this->y = $addressesEndY;

                    /*$page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                    $page->setLineWidth(0.5);
                    $page->drawRectangle(25, $this->y, 275, $this->y - 25);
                    $page->drawRectangle(275, $this->y, 570, $this->y - 25);*/

                    $this->y -= 15;
                    $this->_setFontBold($page, 12);
                    $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
                    //$page->drawText(__('Payment Method:'), 35, $this->y, 'UTF-8');
                    //$page->drawText(__('Shipping Method:'), 285, $this->y, 'UTF-8');

                    $this->y -= 10;
                    $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

                    //$this->_setFontRegular($page, 10);
                    $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);
                    $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

                    $paymentLeft = 35;
                    $yPayments = $this->y + 25;
                } else {
                    $yPayments = $addressesStartY;
                    $paymentLeft = 285;
                }

                foreach ($payment as $value) {
                    if (trim($value) != '') {
                        //Printing "Payment Method" lines
                        $value = preg_replace('/<br[^>]*>/i', "\n",$value);
                        foreach ($this->string->split($value, 45, true, true) as $_value) {
                            $page->drawText(strip_tags(trim($_value)), $paymentLeft, $yPayments, 'UTF-8');
                            $yPayments -= 10;
                        }
                    }
                }

                if ($order->getIsVirtual()) {
                    // replacement of Shipments-Payments rectangle block
                    $yPayments = min($addressesEndY, $yPayments);
                    $page->drawLine(25, $top - 25, 25, $yPayments);
                    $page->drawLine(570, $top - 25, 570, $yPayments);
                    $page->drawLine(25, $yPayments, 570, $yPayments);

                    $this->y = $yPayments - 15;
                } else {
                    $topMargin = 15;
                    $methodStartY = $this->y;
                    $this->y -= 15;

                    /*foreach ($this->string->split($shippingMethod, 45, true, true) as $_value) {
                        $page->drawText(strip_tags(trim($_value)), 285, $this->y, 'UTF-8');
                        $this->y -= 15;
                    }*/

                    $yShipments = $this->y;
                    /*$totalShippingChargesText = "("
                        . __('Total Shipping Charges')
                        . " "
                        . $order->formatPriceTxt($order->getShippingAmount())
                        . ")";

                    $page->drawText($totalShippingChargesText, 285, $yShipments - $topMargin, 'UTF-8');
                    $yShipments -= $topMargin + 10;*/

                    $tracks = [];
                    if ($shipment) {
                        $tracks = $shipment->getAllTracks();
                    }
                    if (count($tracks)) {
                        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                        $page->setLineWidth(0.5);
                        //$page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
                        $page->drawLine(400, $yShipments, 400, $yShipments - 10);
                        //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

                        $this->_setFontRegular($page, 9);
                        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
                        //$page->drawText(__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
                        $page->drawText(__('Title'), 290, $yShipments - 7, 'UTF-8');
                        $page->drawText(__('Number'), 410, $yShipments - 7, 'UTF-8');

                        $yShipments -= 20;
                        //$this->_setFontRegular($page, 8);
                        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 8);
                        foreach ($tracks as $track) {
                            $maxTitleLen = 45;
                            $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
                            $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen) . $endOfTitle;
                            $page->drawText($truncatedTitle, 292, $yShipments, 'UTF-8');
                            $page->drawText($track->getNumber(), 410, $yShipments, 'UTF-8');
                            $yShipments -= $topMargin - 5;
                        }
                    } else {
                        //$yShipments -= $topMargin - 5;
                    }

                    //$currentY = min($yPayments, $yShipments);
                    $currentY = min($yPayments, $yShipments);

                    // replacement of Shipments-Payments rectangle block
                    //$page->drawLine(25, $methodStartY, 25, $currentY);
                    //left
                    //$page->drawLine(25, $currentY, 570, $currentY);
                    //bottom
                    //$page->drawLine(570, $currentY, 570, $methodStartY);
                    //right

                    $this->y = $currentY;
                    $this->y -= 15;
                }
        }
        protected function insertLogo(&$page, $store = null)
        {
            $this->y = $this->y ? $this->y : 815;
            $image = $this->_scopeConfig->getValue(
                'sales/identity/logo',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            );
            if ($image) {
                $imagePath = '/sales/store/logo/' . $image;
               /* if ($this->fileStorageDatabase->checkDbUsage() &&
                    !$this->_mediaDirectory->isFile($imagePath)
                ) {
                    $this->fileStorageDatabase->saveFileToFilesystem($imagePath);
                }*/
                if ($this->_mediaDirectory->isFile($imagePath)) {
                    $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));
                    $top = 830;
                    //top border of the page
                    $widthLimit = 190;
                    //half of the page width
                    $heightLimit = 190;
                    //assuming the image is not a "skyscraper"
                    $width = $image->getPixelWidth();
                    $height = $image->getPixelHeight();

                    //preserving aspect ratio (proportions)
                    $ratio = $width / $height;
                    if ($ratio > 1 && $width > $widthLimit) {
                        $width = $widthLimit;
                        $height = $width / $ratio;
                    } elseif ($ratio < 1 && $height > $heightLimit) {
                        $height = $heightLimit;
                        $width = $height * $ratio;
                    } elseif ($ratio == 1 && $height > $heightLimit) {
                        $height = $heightLimit;
                        $width = $widthLimit;
                    }

                    $y1 = $top - $height;
                    $y2 = $top;
                    $x1 = 25;
                    $x2 = $x1 + $width;

                    //coordinates after transformation are rounded by Zend
                    $page->drawImage($image, $x1, $y1, $x2, $y2);

                    $this->y = $y1 - 10;
                }
            }
        }
        protected function insertTotals($page, $source)
        {
            $order = $source->getOrder();
            $totals = $this->_getTotalsList();
            $lineBlock = ['lines' => [], 'height' => 15];
            foreach ($totals as $total) {
                $total->setOrder($order)->setSource($source);

                if ($total->canDisplay()) {
                    $total->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10);
                    foreach ($total->getTotalsForDisplay() as $totalData) {
                        if($totalData['label'] == 'Subtotal:'){
                            $totallabel = 'BASE IMPONIBLE:'; 
                            
                        }elseif($totalData['label'] == 'Shipping & Handling:'){
                            
                                $totallabel = 'Gastos de envío:';
                            
                        }elseif($totalData['label'] == 'Grand Total:'){
                            
                                $totallabel = 'IMPORTE TOTAL:';
                        }else{
                               $totallabel = $totalData['label'];
                        }
                        //echo '<pre>'; print_r($total->getTotalsForDisplay()); 
                        if($totalData['label'] != "Tax:"){
                        $lineBlock['lines'][] = [
                            [
                                'text' => $totallabel,
                                'feed' => 475,
                                'align' => 'right',
                                'font_size' => $totalData['font_size'],
                                'font' => 'bold',
                            ],
                            [
                                'text' => $totalData['amount'],
                                'feed' => 565,
                                'align' => 'right',
                                'font_size' => $totalData['font_size'],
                                'font' => 'bold'
                            ],
                        ];
                     }
                    }
                }
            }
            

            $this->y -= 20;
            $page = $this->drawLineBlocks($page, [$lineBlock]);
            return $page;
        }
}
