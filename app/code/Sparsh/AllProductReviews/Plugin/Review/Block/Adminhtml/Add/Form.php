<?php
/**
 * Class Form
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_AllProductReviews
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\AllProductReviews\Plugin\Review\Block\Adminhtml\Add;

/**
 * Class Form
 *
 * @category Sparsh
 * @package  Sparsh_AllProductReviews
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class Form extends \Magento\Review\Block\Adminhtml\Add\Form
{
    /**
     * Add position field to the review edit form
     *
     * @param \Magento\Review\Block\Adminhtml\Add\Form $object object
     * @param \Magento\Framework\Data\Form             $form   form
     * 
     * @return \Magento\Framework\Data\Form
     */
    public function beforeSetForm(
        \Magento\Review\Block\Adminhtml\Add\Form $object, 
        $form
    ) {

        $fieldset = $form->getElement('add_review_form');

        $fieldset->addField(
            'position',
            'text',
            ['label' => __('Position'), 'required' => false, 'name' => 'position']
        );

        return [$form];
    }
}
