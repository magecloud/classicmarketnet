<?php
/**
 * Sparsh AllProductReviews Extension
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_AllProductReviews
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Sparsh_AllProductReviews',
    __DIR__
);
