<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionAdvancedPricing\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use MageWorx\OptionAdvancedPricing\Model\TierPrice;
use MageWorx\OptionAdvancedPricing\Model\SpecialPrice;

class ConvertMageWorxIds implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * ConvertMageWorxIds constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $connection->beginTransaction();
        try {
            $this->convertOptionTypeSpecialPriceMageWorxIds();
            $this->convertOptionTypeTierPriceMageWorxIds();
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            throw($e);
        }

    }

    /**
     * Out if column doesn't exist
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    protected function out($table, $column)
    {
        return !$this->moduleDataSetup->getConnection()->tableColumnExists(
            $this->moduleDataSetup->getTable($table),
            $column
        );
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option type special price
     */
    protected function convertOptionTypeSpecialPriceMageWorxIds()
    {
        $tableNames = [
            SpecialPrice::TABLE_NAME                 => 'catalog_product_option_type_value',
            SpecialPrice::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, SpecialPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID)) {
                continue;
            }

            $select = $this->moduleDataSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpotv' => $this->moduleDataSetup->getTable($joinedTable)
                    ],
                    'cpotv.' . SpecialPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID
                    . ' = option_type_special_price.' . SpecialPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID,
                    [
                        SpecialPrice::COLUMN_OPTION_TYPE_ID => SpecialPrice::COLUMN_OPTION_TYPE_ID
                    ]
                )
                ->where(
                    "option_type_special_price." . SpecialPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID . " IS NOT NULL"
                );

            $update = $this->moduleDataSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    [
                        'option_type_special_price' => $this->moduleDataSetup->getTable($mainTable)
                    ]
                );
            $this->moduleDataSetup->getConnection()->query($update);
        }
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option type tier price
     */
    protected function convertOptionTypeTierPriceMageWorxIds()
    {
        $tableNames = [
            TierPrice::TABLE_NAME                 => 'catalog_product_option_type_value',
            TierPrice::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, TierPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID)) {
                continue;
            }

            $select = $this->moduleDataSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpotv' => $this->moduleDataSetup->getTable($joinedTable)
                    ],
                    'cpotv.' . TierPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID
                    . ' = option_type_tier_price.' . TierPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID,
                    [
                        TierPrice::COLUMN_OPTION_TYPE_ID => TierPrice::COLUMN_OPTION_TYPE_ID
                    ]
                )
                ->where(
                    "option_type_tier_price." . TierPrice::COLUMN_MAGEWORX_OPTION_TYPE_ID . " IS NOT NULL"
                );

            $update = $this->moduleDataSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    [
                        'option_type_tier_price' => $this->moduleDataSetup->getTable($mainTable)
                    ]
                );
            $this->moduleDataSetup->getConnection()->query($update);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.1';
    }
}
