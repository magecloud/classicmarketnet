<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionLink\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use MageWorx\OptionInventory\Model\ResourceModel\Product\Option\Value\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Option\Value\CollectionFactory as OptionValueCollectionFactory;
use MageWorx\OptionLink\Helper\Attribute as HelperAttribute;
use MageWorx\OptionLink\Model\ResourceModel\Product\CollectionUpdaterStock;
use MageWorx\OptionBase\Helper\Data as BaseHelper;

/**
 * Class OrderManagement
 */
class UpdateLinkedValueQtyAfterPlaceOrder
{
    /**
     * @var OptionValueCollectionFactory
     */
    protected $optionValueCollectionFactory;

    /**
     * @var Collection
     */
    protected $inventoryValueCollection;

    /**
     * @var \MageWorx\OptionLink\Helper\Attribute
     */
    protected $helperAttribute;

    /**
     * @var CollectionUpdaterStock
     */
    protected $collectionUpdaterStock;

    /**
     * @var BaseHelper
     */
    protected $baseHelper;

    /**
     * UpdateLinkedValueQtyAfterPlaceOrder constructor.
     *
     * @param Collection $inventoryValueCollection
     * @param OptionValueCollectionFactory $optionValueCollectionFactory
     * @param HelperAttribute $helperAttribute
     * @param CollectionUpdaterStock $collectionUpdaterStock
     * @param ProductRepositoryInterface $productRepository
     * @param BaseHelper $baseHelper
     */
    public function __construct(
        Collection $inventoryValueCollection,
        OptionValueCollectionFactory $optionValueCollectionFactory,
        HelperAttribute $helperAttribute,
        CollectionUpdaterStock $collectionUpdaterStock,
        ProductRepositoryInterface $productRepository,
        BaseHelper $baseHelper
    ) {
        $this->inventoryValueCollection     = $inventoryValueCollection;
        $this->optionValueCollectionFactory = $optionValueCollectionFactory;
        $this->helperAttribute              = $helperAttribute;
        $this->collectionUpdaterStock       = $collectionUpdaterStock;
        $this->baseHelper                   = $baseHelper;
    }

    /**
     * @param \Magento\Quote\Model\QuoteManagement $subject
     * @param \Magento\Sales\Model\Order $result
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $orderData
     * @return \Magento\Sales\Model\Order
     */
    public function afterSubmit(
        \Magento\Quote\Model\QuoteManagement $subject,
        \Magento\Sales\Model\Order $result,
        \Magento\Quote\Model\Quote $quote,
        $orderData = []
    ) {
        $linkedFields = $this->helperAttribute->getConvertedAttributesToFields();
        if (!in_array('qty', $linkedFields) ||
            !$this->baseHelper->isModuleEnabled('Magento_InventorySalesAdminUi')
        ) {
            return $result;
        }

        $quoteItems = $quote->getAllItems();
        /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if (!$this->validateItem($quoteItem)) {
                continue;
            }
            $buyRequest = $quoteItem->getBuyRequest();

            /** @var array $options */
            $options = $buyRequest->getOptions();
            if ($options) {
                if (!is_array($options)) {
                    continue;
                }
                $values = $this->getValuesItems(array_keys($options));

                $linkedValueQtyToUpdate = [];
                foreach ($options as $optionId => $optionValue) {

                    if (!is_array($optionValue)) {
                        $optionValues = explode(',', $optionValue);
                    } else {
                        $optionValues = $optionValue;
                    }

                    foreach ($optionValues as $valueId) {
                        $value = isset($values[$valueId]) ? $values[$valueId] : null;
                        if (!$value) {
                            continue;
                        }
                        if ($value->getSkuIsValid()) {
                            $linkedValueQtyToUpdate[] = [
                                'option_type_id' => $value->getOptionTypeId(),
                                'qty'            => $this->getCurrentSalableQty($value->getSku())
                            ];
                        }
                    }
                }
                $this->collectionUpdaterStock->updateLinkedValueQtyAfterPlaceOrder($linkedValueQtyToUpdate);
            } else {
                $productSku = $quoteItem->getSku();
                $this->collectionUpdaterStock->updateLinkedValueQty(
                    $productSku,
                    $this->getCurrentSalableQty($productSku)
                );
            }

        }

        return $result;
    }

    /**
     * @param string $sku
     * @return float
     */
    protected function getCurrentSalableQty(string $sku): float
    {
        return $this->baseHelper->updateValueQtyToSalableQty($sku);
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return bool
     */
    protected function validateItem(\Magento\Quote\Model\Quote\Item $quoteItem): bool
    {
        $buyRequest = $quoteItem->getBuyRequest();
        if (!$buyRequest) {
            return false;
        }

        return true;
    }

    /**
     * Get all options values
     *
     * @param $optionIds
     * @return array
     */
    protected function getValuesItems(array $optionIds): array
    {
        $optionValueCollection = $this->optionValueCollectionFactory->create();
        $valuesItems           = $optionValueCollection->addOptionToFilter($optionIds)->getItems();

        return $valuesItems;
    }
}
