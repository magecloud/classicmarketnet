<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionLink\Model\ResourceModel\Product;

use Magento\Framework\App\ResourceConnection;

class CollectionUpdaterStock
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * CollectionUpdaterStock constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param string $productSku
     * @param float $qty
     */
    public function updateLinkedValueQty(string $productSku, float $qty): void
    {
        $connection    = $this->resourceConnection->getConnection();
        $tableName     = $this->resourceConnection->getTableName('catalog_product_option_type_value');
        $optionTypeIds = $this->getCollectionLinkedValues($productSku, $tableName);

        if ($optionTypeIds) {
            $connection->update(
                $tableName,
                ['qty' => $qty],
                "option_type_id IN (" . $optionTypeIds . ")"
            );
        }
    }

    /**
     * @param string $productSku
     * @param string $tableName
     * @return string
     */
    public function getCollectionLinkedValues(string $productSku, string $tableName): string
    {
        $connection    = $this->resourceConnection->getConnection();
        $optionTypeIds = $connection->fetchCol(
            $connection->select()
                       ->from($tableName, 'option_type_id')
                       ->where('sku = "' . $productSku . '"')
                       ->where('manage_stock = 1')
        );

        return implode(", ", $optionTypeIds);
    }

    /**
     * @param $updateData
     */
    public function updateLinkedValueQtyAfterPlaceOrder(array $updateData): void
    {
        foreach ($updateData as $item) {
            $connection = $this->resourceConnection->getConnection();
            $tableName  = $this->resourceConnection->getTableName('catalog_product_option_type_value');
            $connection->update(
                $tableName,
                ['qty' => $item['qty']],
                "option_type_id = '" . $item['option_type_id'] . "'"
            );
        }
    }
}
