<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See https://www.mageworx.com/terms-and-conditions for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionLink\Model;

use Magento\Catalog\Model\Product;

interface OptionValueAttributeUpdaterInterface
{
    /**
     * @param array $optionTypeIds
     * @param Product $product
     * @return bool
     */
    public function process(array $optionTypeIds, Product $product): bool;
}
