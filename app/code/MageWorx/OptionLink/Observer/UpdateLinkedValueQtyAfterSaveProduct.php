<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionLink\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use MageWorx\OptionLink\Model\ResourceModel\Product\CollectionUpdaterStock;
use MageWorx\OptionLink\Helper\Attribute as HelperAttribute;
use MageWorx\OptionBase\Helper\Data as BaseHelper;


class UpdateLinkedValueQtyAfterSaveProduct implements ObserverInterface
{
    /**
     * @var CollectionUpdaterStock
     */
    protected $collectionUpdaterStock;

    /**
     * @var BaseHelper
     */
    protected $baseHelper;

    /**
     * UpdateLinkedValueQtyAfterSaveProduct constructor.
     *
     * @param CollectionUpdaterStock $collectionUpdaterStock
     */
    public function __construct(
        CollectionUpdaterStock $collectionUpdaterStock,
        BaseHelper $baseHelper
    ) {
        $this->collectionUpdaterStock = $collectionUpdaterStock;
        $this->baseHelper             = $baseHelper;
    }

    /**
     * link salable qty - temporary solution
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        $product    = $observer->getProduct();
        $productSku = $product->getSku();

        if (!$product->getHasOptions()
            && $product->getTypeId() !== \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE
            && $this->baseHelper->isModuleEnabled('Magento_InventorySalesAdminUi')
        ) {
            $this->collectionUpdaterStock->updateLinkedValueQty(
                $productSku,
                $this->baseHelper->updateValueQtyToSalableQty($productSku)
            );
        }
    }
}
