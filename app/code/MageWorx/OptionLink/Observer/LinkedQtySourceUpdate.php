<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionLink\Observer;


use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use MageWorx\OptionLink\Model\ResourceModel\Product\CollectionUpdaterStock;
use MageWorx\OptionLink\Helper\Attribute as HelperAttribute;
use MageWorx\OptionLink\Model\OptionValue;
use MageWorx\OptionBase\Helper\Data as BaseHelper;


class LinkedQtySourceUpdate implements ObserverInterface
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * @var CollectionUpdaterStock
     */
    protected $collectionUpdaterStock;

    /**
     * @var \MageWorx\OptionLink\Helper\Attribute
     */
    protected $helperAttribute;

    /**
     * @var BaseHelper
     */
    protected $baseHelper;

    /**
     * LinkedQtySourceUpdate constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        CollectionUpdaterStock $collectionUpdaterStock,
        HelperAttribute $helperAttribute,
        BaseHelper $baseHelper
    ) {
        $this->objectManager          = $objectManager;
        $this->collectionUpdaterStock = $collectionUpdaterStock;
        $this->helperAttribute        = $helperAttribute;
        $this->baseHelper             = $baseHelper;
    }

    /**
     * link salable qty - temporary solution
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        $linkedFields = $this->helperAttribute->getConvertedAttributesToFields();
        if (!in_array('qty', $linkedFields)) {
            return;
        }

        $values = $observer->getDataToUpdate();
        foreach ($values as $value) {
            if ($value->getSkuIsValid()) {
                $optionId     = $value->getOptionId();
                $option       = $observer->getOptionCollection()->getItemById($optionId);
                $defaultStock = $this->baseHelper->updateValueQtyToSalableQty($value->getSku());
                $value->setQty($defaultStock);

                if ($option) {
                    $option->addValue($value);
                    $value->setOption($option);
                }
            }
        }
    }
}