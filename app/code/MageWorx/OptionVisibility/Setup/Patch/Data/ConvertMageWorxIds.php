<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionVisibility\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use MageWorx\OptionVisibility\Model\OptionCustomerGroup;
use MageWorx\OptionVisibility\Model\OptionStoreView;

class ConvertMageWorxIds implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * ConvertMageWorxIds constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $connection->beginTransaction();
        try {
            $this->convertOptionCustomerGroupMageWorxIds();
            $this->convertOptionTypeTierPriceMageWorxIds();
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            throw($e);
        }

    }

    /**
     * Out if column doesn't exist
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    protected function out($table, $column)
    {
        return !$this->moduleDataSetup->getConnection()->tableColumnExists($this->moduleDataSetup->getTable($table), $column);
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option customer group
     */
    protected function convertOptionCustomerGroupMageWorxIds()
    {
        $tableNames = [
            OptionCustomerGroup::TABLE_NAME                 => 'catalog_product_option',
            OptionCustomerGroup::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, OptionCustomerGroup::COLUMN_NAME_MAGEWORX_OPTION_ID)) {
                continue;
            }

            $select = $this->moduleDataSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpo' => $this->moduleDataSetup->getTable($joinedTable)
                    ],
                    'cpo.' . OptionCustomerGroup::COLUMN_NAME_MAGEWORX_OPTION_ID
                    . ' = option_customer_group.' . OptionCustomerGroup::COLUMN_NAME_MAGEWORX_OPTION_ID,
                    [
                        OptionCustomerGroup::COLUMN_NAME_OPTION_ID => OptionCustomerGroup::COLUMN_NAME_OPTION_ID
                    ]
                )
                ->where(
                    "option_customer_group." . OptionCustomerGroup::COLUMN_NAME_MAGEWORX_OPTION_ID . " IS NOT NULL"
                );

            $update = $this->moduleDataSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    [
                        'option_customer_group' => $this->moduleDataSetup->getTable($mainTable)
                    ]
                );
            $this->moduleDataSetup->getConnection()->query($update);
        }
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option store view
     */
    protected function convertOptionTypeTierPriceMageWorxIds()
    {
        $tableNames = [
            OptionStoreView::TABLE_NAME                 => 'catalog_product_option',
            OptionStoreView::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, OptionStoreView::COLUMN_NAME_MAGEWORX_OPTION_ID)) {
                continue;
            }

            $select = $this->moduleDataSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpo' => $this->moduleDataSetup->getTable($joinedTable)
                    ],
                    'cpo.' . OptionStoreView::COLUMN_NAME_MAGEWORX_OPTION_ID
                    . ' = option_store_view.' . OptionStoreView::COLUMN_NAME_MAGEWORX_OPTION_ID,
                    [
                        OptionStoreView::COLUMN_NAME_OPTION_ID => OptionStoreView::COLUMN_NAME_OPTION_ID
                    ]
                )
                ->where(
                    "option_store_view." . OptionStoreView::COLUMN_NAME_MAGEWORX_OPTION_ID . " IS NOT NULL"
                );

            $update = $this->moduleDataSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    [
                        'option_store_view' => $this->moduleDataSetup->getTable($mainTable)
                    ]
                );
            $this->moduleDataSetup->getConnection()->query($update);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.1';
    }
}
