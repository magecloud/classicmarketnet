<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionFeatures\Plugin\Refund;


use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use MageWorx\OptionFeatures\Model\QtyMultiplier;

class QtyMultiplierSourceDeductionService
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var QtyMultiplier
     */
    protected $qtyMultiplier;

    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * QtyMuliptilerSourceDeductionService constructor.
     *
     * @param Registry $registry
     * @param QtyMultiplier $qtyMultiplier
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Registry $registry,
        QtyMultiplier $qtyMultiplier,
        ObjectManagerInterface $objectManager
    ) {
        $this->registry      = $registry;
        $this->qtyMultiplier = $qtyMultiplier;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritdoc
     */
    public function beforeExecute(
        \Magento\InventorySourceDeductionApi\Model\SourceDeductionService $subject,
        \Magento\InventorySourceDeductionApi\Model\SourceDeductionRequestInterface $sourceDeductionRequest
    ): void {
        $creditmemo = $this->registry->registry('current_creditmemo');

        if (!$creditmemo) {
            return;
        }

        $sourceItemsSave                 = $this->objectManager->get(
            \Magento\InventoryApi\Api\SourceItemsSaveInterface::class
        );
        $getStockItemConfiguration       = $this->objectManager->get(
            \Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface::class
        );
        $getStockBySalesChannel          = $this->objectManager->get(
            \Magento\InventorySalesApi\Api\GetStockBySalesChannelInterface::class
        );
        $getSourceItemBySourceCodeAndSku = $this->objectManager->get(
            \Magento\InventorySourceDeductionApi\Model\GetSourceItemBySourceCodeAndSku::class
        );

        $currentItems = $creditmemo->getAllItems();
        $sourceItems  = [];
        $sourceCode   = $sourceDeductionRequest->getSourceCode();
        $salesChannel = $sourceDeductionRequest->getSalesChannel();
        $stockId      = $getStockBySalesChannel->execute($salesChannel)->getStockId();
        $qtyToDeduct  = [];
        foreach ($currentItems as $item) {
            $itemSku                = $item->getSku();
            $qty                    = $item->getQty();
            $stockItemConfiguration = $getStockItemConfiguration->execute(
                $itemSku,
                $stockId
            );

            if (!$stockItemConfiguration->isManageStock()) {
                //We don't need to Manage Stock
                continue;
            }

            $currentQtyMultiplierQty = $this->qtyMultiplier->getQtyMultiplierQtyForCurrentItemQty(
                $item->getOrderItem(),
                $qty
            );

            if (!$currentQtyMultiplierQty) {
                continue;
            }

            $qty = ($currentQtyMultiplierQty - $qty) * (-1);

            if (isset($qtyToDeduct[$itemSku])) {
                $qtyToDeduct[$itemSku] += $qty;
            } else {
                $qtyToDeduct[$itemSku] = $qty;
            }
        }

        foreach ($currentItems as $item) {
            $itemSku    = $item->getSku();
            $sourceItem = $getSourceItemBySourceCodeAndSku->execute($sourceCode, $itemSku);
            if (($sourceItem->getQuantity() - $qtyToDeduct[$itemSku]) >= 0) {
                $sourceItem->setQuantity($sourceItem->getQuantity() - $qtyToDeduct[$itemSku]);
                $sourceItems[] = $sourceItem;
            } else {
                throw new LocalizedException(
                    __('Not all of your products are available in the requested quantity.')
                );
            }
        }


        if (!empty($sourceItems)) {
            $sourceItemsSave->execute($sourceItems);
        }
    }
}