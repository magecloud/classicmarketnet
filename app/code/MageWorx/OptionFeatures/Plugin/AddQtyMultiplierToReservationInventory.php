<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionFeatures\Plugin;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Checkout\Model\Session;
use Magento\Backend\Model\Session\Quote as BackendQuoteSession;
use MageWorx\OptionBase\Helper\System as SystemHelper;
use MageWorx\OptionBase\Helper\Data as BaseHelper;
use MageWorx\OptionFeatures\Model\QtyMultiplier;
use Magento\Sales\Api\Data\ShipmentInterface;
use MageWorx\OptionFeatures\Plugin\Shipment\RegisterCurrentShipmentAfterSavePlugin;

class AddQtyMultiplierToReservationInventory
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var BackendQuoteSession
     */
    protected $backendQuoteSession;

    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * @var SystemHelper
     */
    protected $systemHelper;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var BaseHelper
     */
    protected $baseHelper;

    /**
     * @var QtyMultiplier
     */
    protected $qtyMultiplier;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param SerializerInterface $serializer
     * @param Session $checkoutSession
     * @param BackendQuoteSession $backendQuoteSession
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param ObjectManagerInterface $objectManager
     * @param SystemHelper $systemHelper
     * @param Registry $registry
     * @param BaseHelper $baseHelper
     * @param QtyMultiplier $qtyMultiplier
     * @param \Psr\Log\LoggerInterface $logger
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        SerializerInterface $serializer,
        Session $checkoutSession,
        BackendQuoteSession $backendQuoteSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        ObjectManagerInterface $objectManager,
        SystemHelper $systemHelper,
        Registry $registry,
        BaseHelper $baseHelper,
        QtyMultiplier $qtyMultiplier,
        \Psr\Log\LoggerInterface $logger,
        DataPersistorInterface $dataPersistor
    ) {
        $this->serializer          = $serializer;
        $this->checkoutSession     = $checkoutSession;
        $this->backendQuoteSession = $backendQuoteSession;
        $this->orderRepository     = $orderRepository;
        $this->objectManager       = $objectManager;
        $this->systemHelper        = $systemHelper;
        $this->registry            = $registry;
        $this->baseHelper          = $baseHelper;
        $this->qtyMultiplier       = $qtyMultiplier;
        $this->logger              = $logger;
        $this->dataPersistor       = $dataPersistor;
    }

    /**
     * @param \Magento\InventorySales\Model\PlaceReservationsForSalesEvent $subject
     * @param \Closure $proceed
     * @param array $items
     * @param \Magento\InventorySalesApi\Api\Data\SalesChannelInterface $salesChannel
     * @param $salesEvent
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundExecute(
        \Magento\InventorySales\Model\PlaceReservationsForSalesEvent $subject,
        \Closure $proceed,
        array $items,
        \Magento\InventorySalesApi\Api\Data\SalesChannelInterface $salesChannel,
        \Magento\InventorySales\Model\SalesEvent $salesEvent
    ) {
        if (empty($items)) {
            return $proceed($items, $salesChannel, $salesEvent);
        }

        $reservationBuilder                          = $this->objectManager->get(
            \Magento\InventoryReservationsApi\Model\ReservationBuilderInterface::class
        );
        $appendReservations                          = $this->objectManager->get(
            \Magento\InventoryReservationsApi\Model\AppendReservationsInterface::class
        );
        $getStockBySalesChannel                      = $this->objectManager->get(
            \Magento\InventorySalesApi\Api\GetStockBySalesChannelInterface::class
        );
        $getProductTypesBySkus                       = $this->objectManager->get(
            \Magento\InventoryCatalogApi\Model\GetProductTypesBySkusInterface::class
        );
        $isSourceItemManagementAllowedForProductType = $this->objectManager->get(
            \Magento\InventoryConfigurationApi\Model\IsSourceItemManagementAllowedForProductTypeInterface::class
        );
        $salesEventToArrayConverter                  = $this->objectManager->get(
            \Magento\InventorySales\Model\SalesEventToArrayConverter::class
        );

        $stockId         = $getStockBySalesChannel->execute($salesChannel)->getStockId();
        $isShipmentEvent = $salesEvent->getType() == 'shipment_created' ?? false;
        $isOrderEvent    = $salesEvent->getType() == 'order_placed' ?? false;

        $currentItems = $this->getCurrentItemsFromSource($salesEvent, $isShipmentEvent, $isOrderEvent);

        if (!$currentItems) {
            return $proceed($items, $salesChannel, $salesEvent);
        }

        $qtyMultiplierData = $this->getMultiplierDataToApply($currentItems, $isShipmentEvent);

        $skus = [];
        /** @var \Magento\InventorySalesApi\Api\Data\ItemToSellInterface $item */
        foreach ($items as $item) {
            $skus[] = $item->getSku();
        }
        $productTypes = $getProductTypesBySkus->execute($skus);

        $reservations = [];
        $multiplier   = $isOrderEvent ? -1 : 1;
        /** @var /Magento\InventorySalesApi\Api\Data\ItemToSellInterface $item */
        foreach ($items as $item) {
            $currentSku = $item->getSku();
            if (isset($qtyMultiplierData[$currentSku])) {
                $qtyToReservation = $qtyMultiplierData[$currentSku]['qty_multiplier_qty'] * $multiplier;
            } else {
                $qtyToReservation = (float)$item->getQuantity();
            }

            $skuNotExistInCatalog = !isset($productTypes[$currentSku]);
            if ($skuNotExistInCatalog ||
                $isSourceItemManagementAllowedForProductType->execute($productTypes[$currentSku])) {
                $reservations[] = $reservationBuilder
                    ->setSku($item->getSku())
                    ->setQuantity($qtyToReservation)
                    ->setStockId($stockId)
                    ->setMetadata($this->serializer->serialize($salesEventToArrayConverter->execute($salesEvent)))
                    ->build();
            }
        }
        $appendReservations->execute($reservations);
    }

    /**
     * @param \Magento\InventorySales\Model\SalesEvent $salesEvent
     * @param bool $isShipmentEvent
     * @param bool $isOrderEvent
     * @return array
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCurrentItemsFromSource(
        \Magento\InventorySales\Model\SalesEvent $salesEvent,
        bool $isShipmentEvent,
        bool $isOrderEvent
    ): array {
        if ($salesEvent->getType() == 'order_placed') {
            if ($this->systemHelper->isAdmin()) {
                $currentItems = $this->backendQuoteSession->getQuote()->getAllVisibleItems();
            } else {
                $currentItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
            }
        } else {
            if ($isShipmentEvent) {
                try {
                    $shipment     = $this->getCurrentShipment();
                    $currentItems = $shipment->getAllItems();
                } catch (LocalizedException $exception) {
                    $this->logger->error($exception->getLogMessage());

                    return [];
                }
            } else {
                $currentItems = $this->orderRepository->get($salesEvent->getObjectId())->getItems();
            }
        }

        return $currentItems;
    }

    /**
     * @param array $currentItems
     * @param bool $isShipmentEvent
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function getMultiplierDataToApply(array $currentItems, bool $isShipmentEvent): array
    {
        $qtyMultiplierData = [];

        foreach ($currentItems as $currentItem) {
            if ($isShipmentEvent) {
                $orderItem               = $currentItem->getOrderItem();
                $currentQtyMultiplierQty = $this->qtyMultiplier->getQtyMultiplierQtyForCurrentItemQty(
                    $orderItem,
                    $currentItem->getQty()
                );
                $originalProductSku      = $currentItem->getOrderItem()->getSku();
            } else {
                $currentQtyMultiplierQty = $currentItem->getBuyRequest()->getQtyMultiplierQty();
                $originalProductSku      = $currentItem->getProduct()->getData('sku');
            }

            if (is_null($currentQtyMultiplierQty) || empty($currentQtyMultiplierQty)) {
                $currentQtyMultiplierQty = $currentItem->getQty();
            }

            if (isset($qtyMultiplierData[$originalProductSku]['qty_multiplier_qty'])) {
                $currentQtyMultiplierQty += $qtyMultiplierData[$originalProductSku]['qty_multiplier_qty'];
            }
            $qtyMultiplierData[$originalProductSku] = ['qty_multiplier_qty' => $currentQtyMultiplierQty];
        }

        return $qtyMultiplierData;
    }

    /**
     * @return ShipmentInterface
     */
    protected function getCurrentShipment(): ShipmentInterface
    {
        $shipment = $this->registry->registry('current_shipment');

        return $shipment ?: $this->dataPersistor->get(RegisterCurrentShipmentAfterSavePlugin::SHIPMENT_KEY);
    }
}
