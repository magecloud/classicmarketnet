<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionFeatures\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use MageWorx\OptionFeatures\Model\Image;
use MageWorx\OptionFeatures\Model\OptionDescription;
use MageWorx\OptionFeatures\Model\OptionTypeDescription;
use MageWorx\OptionFeatures\Model\OptionTypeIsDefault;


class ConvertMageWorxIds implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var SchemaSetupInterface
     */
    private $schemaSetup;

    /**
     * EnableSegmentation constructor.
     *
     * @param SchemaSetupInterface $schemaSetup
     */
    public function __construct(
        SchemaSetupInterface $schemaSetup
    ) {
        $this->schemaSetup = $schemaSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->schemaSetup->getConnection()->beginTransaction();
        try {
            $this->convertOptionDescriptionMageWorxIds();
            $this->convertOptionTypeDescriptionMageWorxIds();
            $this->convertOptionTypeImageMageWorxIds();
            $this->convertOptionTypeIsDefaultMageWorxIds();
            $this->schemaSetup->getConnection()->commit();
        } catch (\Exception $e) {
            $this->schemaSetup->getConnection()->rollback();
            throw($e);
        }
    }

    /**
     * Out if column doesn't exist
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    protected function out($table, $column)
    {
        return !$this->schemaSetup->getConnection()->tableColumnExists($this->schemaSetup->getTable($table), $column);
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option description
     */
    protected function convertOptionDescriptionMageWorxIds()
    {
        $tableNames = [
            OptionDescription::TABLE_NAME                 => 'catalog_product_option',
            OptionDescription::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, OptionDescription::COLUMN_NAME_MAGEWORX_OPTION_ID)) {
                continue;
            }

            $select = $this->schemaSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpo' => $this->schemaSetup->getTable($joinedTable)
                    ],
                    'cpo.' . OptionDescription::COLUMN_NAME_MAGEWORX_OPTION_ID
                    . ' = option_description.' . OptionDescription::COLUMN_NAME_MAGEWORX_OPTION_ID,
                    [
                        OptionDescription::COLUMN_NAME_OPTION_ID => OptionDescription::COLUMN_NAME_OPTION_ID
                    ]
                )
                ->where(
                    "option_description." . OptionDescription::COLUMN_NAME_MAGEWORX_OPTION_ID . " IS NOT NULL"
                );

            $update = $this->schemaSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    ['option_description' => $this->schemaSetup->getTable($mainTable)]
                );
            $this->schemaSetup->getConnection()->query($update);
        }
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option type description
     */
    protected function convertOptionTypeDescriptionMageWorxIds()
    {
        $tableNames = [
            OptionTypeDescription::TABLE_NAME                 => 'catalog_product_option_type_value',
            OptionTypeDescription::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, OptionTypeDescription::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID)) {
                continue;
            }

            $select = $this->schemaSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpotv' => $this->schemaSetup->getTable($joinedTable)
                    ],
                    'cpotv.' . OptionTypeDescription::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID
                    . ' = option_type_description.' . OptionTypeDescription::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID,
                    [
                        OptionTypeDescription::COLUMN_NAME_OPTION_TYPE_ID => OptionTypeDescription::COLUMN_NAME_OPTION_TYPE_ID
                    ]
                )
                ->where(
                    "option_type_description." . OptionTypeDescription::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID . " IS NOT NULL"
                );

            $update = $this->schemaSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    [
                        'option_type_description' => $this->schemaSetup->getTable($mainTable)
                    ]
                );

            $this->schemaSetup->getConnection()->query($update);
        }
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option type image
     */
    protected function convertOptionTypeImageMageWorxIds()
    {
        $tableNames = [
            Image::TABLE_NAME                 => 'catalog_product_option_type_value',
            Image::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, Image::COLUMN_MAGEWORX_OPTION_TYPE_ID)) {
                continue;
            }

            $select = $this->schemaSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpotv' => $this->schemaSetup->getTable($joinedTable)
                    ],
                    'cpotv.' . Image::COLUMN_MAGEWORX_OPTION_TYPE_ID
                    . ' = option_type_image.' . Image::COLUMN_MAGEWORX_OPTION_TYPE_ID,
                    [
                        Image::COLUMN_OPTION_TYPE_ID => Image::COLUMN_OPTION_TYPE_ID
                    ]
                )
                ->where(
                    "option_type_image." . Image::COLUMN_MAGEWORX_OPTION_TYPE_ID . " IS NOT NULL"
                );

            $update = $this->schemaSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    ['option_type_image' => $this->schemaSetup->getTable($mainTable)]
                );
            $this->schemaSetup->getConnection()->query($update);
        }
    }

    /**
     * Update new option_id with mageworx_option_id equivalent for option type isDefault
     */
    protected function convertOptionTypeIsDefaultMageWorxIds()
    {
        $tableNames = [
            OptionTypeIsDefault::TABLE_NAME                 => 'catalog_product_option_type_value',
            OptionTypeIsDefault::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, OptionTypeIsDefault::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID)) {
                continue;
            }

            $select = $this->schemaSetup
                ->getConnection()
                ->select()
                ->joinLeft(
                    [
                        'cpotv' => $this->schemaSetup->getTable($joinedTable)
                    ],
                    'cpotv.' . OptionTypeIsDefault::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID
                    . ' = option_type_is_default.' . OptionTypeIsDefault::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID,
                    [
                        OptionTypeIsDefault::COLUMN_NAME_OPTION_TYPE_ID => OptionTypeIsDefault::COLUMN_NAME_OPTION_TYPE_ID
                    ]
                )
                ->where(
                    "option_type_is_default." . OptionTypeIsDefault::COLUMN_NAME_MAGEWORX_OPTION_TYPE_ID . " IS NOT NULL"
                );

            $update = $this->schemaSetup
                ->getConnection()
                ->updateFromSelect(
                    $select,
                    ['option_type_is_default' => $this->schemaSetup->getTable($mainTable)]
                );
            $this->schemaSetup->getConnection()->query($update);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.7';
    }
}
