<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Setup\Patch\Schema;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use \MageWorx\OptionDependency\Model\ForeignKeyInstallProcess as ForeignKeyInstallProcess;

class InstallForeignKey implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ForeignKeyInstallProcess
     */
    protected $foreignKeyInstallProcess;

    /**
     * InstallForeignKey constructor.
     *
     * @param ForeignKeyInstallProcess $foreignKeyInstallProcess
     */
    public function __construct(
        ForeignKeyInstallProcess $foreignKeyInstallProcess
    ) {
        $this->foreignKeyInstallProcess = $foreignKeyInstallProcess;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->foreignKeyInstallProcess->installProcess();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.4';
    }
}
