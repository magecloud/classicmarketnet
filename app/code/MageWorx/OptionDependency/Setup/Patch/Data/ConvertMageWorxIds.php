<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use MageWorx\OptionDependency\Model\Config as DependencyModel;

class ConvertMageWorxIds implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * ConvertMageWorxIds constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();

        // If child_option_id isn't exist in table, table has latest schema view (with 'dp' column prefix)
        if (!$this->out(DependencyModel::TABLE_NAME, 'child_option_id')) {
            $this->processFields();
            $connection->beginTransaction();
            try {
                $this->convertDependencyOptionMageWorxIds();
                $this->convertDependencyOptionValueMageWorxIds();
                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollback();
                throw($e);
            }
        }
    }

    /**
     * Out if column doesn't exist
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    protected function out($table, $column)
    {
        return !$this->moduleDataSetup->getConnection()->tableColumnExists(
            $this->moduleDataSetup->getTable($table),
            $column
        );
    }

    /**
     * Process fields due to removing mageworx_ids:
     * Copy old data to temporary field
     * Get option_id/option_type_id equivalent for mageworx_option_id/mageworx_option_type_id
     */
    protected function processFields()
    {
        $tableNames = [
            DependencyModel::TABLE_NAME,
            DependencyModel::OPTIONTEMPLATES_TABLE_NAME
        ];

        foreach ($tableNames as $tableName) {
            $this->moveMageWorxIdsToTemporaryFields($tableName);
        }
    }

    /**
     * @param string $tableName
     */
    protected function moveMageWorxIdsToTemporaryFields($tableName)
    {
        $data = [
            'child_mageworx_option_id'       => new \Zend_Db_Expr('child_option_id'),
            'child_mageworx_option_type_id'  => new \Zend_Db_Expr('child_option_type_id'),
            'parent_mageworx_option_id'      => new \Zend_Db_Expr('parent_option_id'),
            'parent_mageworx_option_type_id' => new \Zend_Db_Expr('parent_option_type_id'),
            'is_processed'                   => 1
        ];
        $this->moduleDataSetup->getConnection()->update(
            $this->moduleDataSetup->getTable($tableName),
            $data,
            ['is_processed' . ' = ?' => '0']
        );
    }


    /**
     * Update new option_id with mageworx_option_id equivalent for dependency child/parent option
     */
    protected function convertDependencyOptionMageWorxIds()
    {
        $tableNames = [
            DependencyModel::TABLE_NAME                 => 'catalog_product_option',
            DependencyModel::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, 'mageworx_option_id')) {
                continue;
            }

            $fieldMap = [
                'child_mageworx_option_id'  => 'child_option_id',
                'parent_mageworx_option_id' => 'parent_option_id'
            ];

            foreach ($fieldMap as $oldColumnName => $newColumnName) {
                $select = $this->moduleDataSetup
                    ->getConnection()
                    ->select()
                    ->joinLeft(
                        [
                            'cpo' => $this->moduleDataSetup->getTable($joinedTable)
                        ],
                        'cpo.mageworx_option_id = option_dependency.' . $oldColumnName,
                        [
                            $newColumnName => 'option_id'
                        ]
                    )
                    ->where(
                        "option_dependency." . $oldColumnName . " IS NOT NULL"
                    );

                $update = $this->moduleDataSetup
                    ->getConnection()
                    ->updateFromSelect(
                        $select,
                        ['option_dependency' => $this->moduleDataSetup->getTable($mainTable)]
                    );
                $this->moduleDataSetup->getConnection()->query($update);
            }
        }
    }

    /**
     * Update new option_type_id with mageworx_option_type_id equivalent for dependency child/parent option value
     */
    protected function convertDependencyOptionValueMageWorxIds()
    {
        $tableNames = [
            DependencyModel::TABLE_NAME                 => 'catalog_product_option_type_value',
            DependencyModel::OPTIONTEMPLATES_TABLE_NAME => 'mageworx_optiontemplates_group_option_type_value'
        ];

        foreach ($tableNames as $mainTable => $joinedTable) {

            if ($this->out($joinedTable, 'mageworx_option_type_id')) {
                continue;
            }

            $fieldMap = [
                'child_mageworx_option_type_id'  => 'child_option_type_id',
                'parent_mageworx_option_type_id' => 'parent_option_type_id'
            ];

            foreach ($fieldMap as $oldColumnName => $newColumnName) {
                $select = $this->moduleDataSetup
                    ->getConnection()
                    ->select()
                    ->joinLeft(
                        [
                            'cpotv' => $this->moduleDataSetup->getTable($joinedTable)
                        ],
                        'cpotv.mageworx_option_type_id = option_value_dependency.' . $oldColumnName,
                        [
                            $newColumnName => 'option_type_id'
                        ]
                    )
                    ->where(
                        "option_value_dependency." . $oldColumnName . " IS NOT NULL"
                    );

                $update = $this->moduleDataSetup
                    ->getConnection()
                    ->updateFromSelect(
                        $select,
                        ['option_value_dependency' => $this->moduleDataSetup->getTable($mainTable)]
                    );
                $this->moduleDataSetup->getConnection()->query($update);
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \MageWorx\OptionDependency\Setup\Patch\Schema\InstallOldColumns::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.7';
    }
}
