<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Model;


use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\SchemaSetupInterface;
use \MageWorx\OptionBase\Helper\Data as HelperBase;

class ForeignKeyInstallProcess
{
    /**
     * @var SchemaSetupInterface
     */
    private $schemaSetup;

    /**
     * @var HelperBase
     */
    private $helperBase;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * ForeignKeyInstallProcess constructor.
     *
     * @param SchemaSetupInterface $schemaSetup
     * @param HelperBase $helperBase
     * @param ResourceConnection $resource
     */
    public function __construct(
        SchemaSetupInterface $schemaSetup,
        HelperBase $helperBase,
        ResourceConnection $resource
    ) {
        $this->schemaSetup  = $schemaSetup;
        $this->helperBase   = $helperBase;
        $this->resource     = $resource;
    }

    /**
     * {@inheritdoc}
     */
    public function installProcess()
    {
        $installer   = $this->resource;
        $connection  = $this->schemaSetup->getConnection();
        $foreignKeys = $this->getForeignKeys();

        foreach ($foreignKeys as $item) {
            // install to Magento
            $tableName = $installer->getTableName($item['table_name']);
            if ($connection->isTableExists($tableName) &&
                !$this->helperBase->isForeignKeyExist($item, $tableName)
            ) {
                $referenceTableName = $installer->getTableName($item['reference_table_name']);

                $fkk = $installer->getFkName(
                    $tableName,
                    $item['column_name'],
                    $referenceTableName,
                    $item['reference_column_name']
                );
                $connection->addForeignKey(
                    $fkk,
                    $tableName,
                    $item['column_name'],
                    $referenceTableName,
                    $item['reference_column_name'],
                    $item['on_delete']
                );
            }
        }
    }

    /**
     * Retrieve module foreign keys data array
     *
     * @return array
     */
    public function getForeignKeys()
    {
        $dataArray = [
            [
                'table_name'            => Config::TABLE_NAME,
                'column_name'           => 'product_id',
                'reference_table_name'  => 'catalog_product_entity',
                'reference_column_name' => $this->helperBase->isEnterprise() ? 'row_id' : 'entity_id',
                'on_delete'             => Table::ACTION_CASCADE
            ],
            [
                'table_name'            => 'mageworx_optiontemplates_group_option_dependency',
                'column_name'           => 'group_id',
                'reference_table_name'  => 'mageworx_optiontemplates_group',
                'reference_column_name' => 'group_id',
                'on_delete'             => Table::ACTION_CASCADE
            ]
        ];

        return $dataArray;
    }
}