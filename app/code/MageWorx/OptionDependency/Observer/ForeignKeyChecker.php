<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\OptionDependency\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use MageWorx\OptionDependency\Model\ForeignKeyInstallProcess as ForeignKeyInstallProcess;

class ForeignKeyChecker implements ObserverInterface
{
    /**
     * @var ForeignKeyInstallProcess
     */
    protected $foreignKeyInstallProcess;

    /**
     * ForeignKeyChecker constructor.
     *
     * @param ForeignKeyInstallProcess $foreignKeyInstallProcess
     */
    public function __construct(
        ForeignKeyInstallProcess $foreignKeyInstallProcess
    ) {
        $this->foreignKeyInstallProcess = $foreignKeyInstallProcess;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $this->foreignKeyInstallProcess->installProcess();
    }
}
